/**
 * Created by xart on 19.01.16..
 */
var express  = require('express');
var app      = express();
var port     = process.env.PORT || 8080;
var mongoose = require('mongoose');
var passport = require('passport');
var flash    = require('connect-flash');

var morgan          = require('morgan');
var cookieParser    = require('cookie-parser');
var bodyParser      = require('body-parser');
var session         = require('express-session');
var methodOverride  = require('method-override');

var configDB = require('./config/database.js');

mongoose.connect(configDB.url);

require('./config/passport')(passport);

// CONNECTION EVENTS
// When successfully connected
mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection open to ' + configDB.url);
});

// If the connection throws an error
mongoose.connection.on('error',function (err) {
    console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection disconnected');
});

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function() {
    mongoose.connection.close(function () {
        console.log('Mongoose default connection disconnected through app termination');
        process.exit(0);
    });
});


    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error.ejs', {
            message: err.message,
            error: err
        });
    });


app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser());
app.use(methodOverride('_method'));

app.set('view engine', 'ejs');

app.use(session({secret: 'someRandomSecretLetsDoItNow' }));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

app.use('/static', express.static('public'));

require('./app/routes/index.js')(app, passport );



app.listen(port);
console.log('The magic happens on port ' + port);