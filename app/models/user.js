/**
 * Created by xart on 19.01.16..
 */
var mongoose    = require('mongoose');
var bcrypt      = require('bcrypt-nodejs');

require('./todoList.js');
require('./task');

var userSchema = mongoose.Schema({
    local : {
        firstName   : String,
        lastName    : String,
        email       : String,
        password    : String,
        listId      : [{ type: mongoose.Schema.Types.ObjectId, ref: 'TodoList'}],
        taskId      : [{ type: mongoose.Schema.Types.ObjectId, ref: 'Task' }]
    }
});

//generate hash
userSchema.methods.generateHash = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

//check if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('User', userSchema);
