/**
 * Created by xart on 19.01.16..
 */
var mongoose = require('mongoose'),
    URLSlugs = require('mongoose-url-slugs');
require('./user');
require('./todoList');

var taskSchema = mongoose.Schema({
    taskName        : String,
    taskDeadline    : Date,
    taskCompleted   : { type: Boolean, default: 0},
    _listId         : [{ type: mongoose.Schema.Types.ObjectId, ref: 'TodoList' }],
    _userId      : [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
});

taskSchema.plugin(URLSlugs('taskName', {field: 'taskSlug'}));
module.exports = mongoose.model('Task', taskSchema);