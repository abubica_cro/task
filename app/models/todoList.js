/**
 * Created by xart on 19.01.16..
 */
var mongoose = require('mongoose'),
    URLSlugs = require('mongoose-url-slugs');

require('./user');
require('./task');

var todoListSchema = mongoose.Schema({
    listName        : String,
    listTimeCreated : { type: Date, default: Date.now },
    _userId         : [{ type: mongoose.Schema.Types.ObjectId, ref: 'User'}]
});

todoListSchema.plugin(URLSlugs('listName', {field: 'listSlug'}));

todoListSchema.pre('remove', function(next) {
    this.model('Task').remove({ _listId: this._id }, next);
});

module.exports = mongoose.model('TodoList', todoListSchema);