/**
 * Created by xart on 24.01.16..
 */
module.exports = function (app, passport) {

    var todo        = require('../../config/todoList');

    app.get ('/todoList', todo.list.showAllLists);
    app.post('/todoList', todo.list.createNewList);
    app.delete('/todoList/:listSlug', todo.list.deleteList);

    app.get('/todoList/:listSlug',todo.task.showAllTasks);
    app.get('/todoList/:listSlug/:taskSlug',todo.task.getTask);
    app.post('/todoList/:listSlug', todo.task.createTask);
    app.put('/todoList/:listSlug/:taskSlug', todo.task.updateTask);
    app.delete('/todoList/:listSlug/:id', todo.task.deleteTask);
    app.post('/todoList/:listSlug/taskCompleted/:id', todo.task.completeTask);

};