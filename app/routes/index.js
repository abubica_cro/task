/**
 * Created by xart on 21.01.16..
 */
module.exports = function(app, passport ) {

    Array.prototype.contains = function(element){
        return this.indexOf(element) > -1;
    };
    var noSecureLinks = ['/', '/login', '/signup', '/static'];

    app.use('*', isLoggedIn);

    function isLoggedIn(req, res, next) {
        if (noSecureLinks.contains(req.originalUrl)) {
            return next();
        } else {
            if (req.isAuthenticated())
                return next();

                res.redirect('/login');
        }
    }

    require('./mainRoutes')     (app, passport );
    require('./userRoutes')     (app, passport );
    require('./todoListRoutes') (app, passport );

};
