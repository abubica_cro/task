/**
 * Created by xart on 23.01.16..
 */

module.exports = function (app, passport) {

    //login
    app.get('/login', function(req, res) {
            if (req.isAuthenticated())
            {
                res.redirect('/todoList');
            }
            else
            {
                res.render('main/login.ejs', { message: req.flash('loginMessage') });
            }
    });

    app.post('/login', passport.authenticate('local-login', {
        successRedirect : '/todoList',
        failureRedirect : '/',
        failureFlash    : true
    }));

    //signup
    app.get('/signup', function(req, res) {
        if (req.isAuthenticated())
        {
            res.redirect('/todoList');
        }
        else
        {
            res.render('main/signup.ejs', {message: req.flash('signupMessage') });

        }
    });

    app.post('/signup', passport.authenticate('local-signup', {

        successRedirect : '/',
        failureRedirect : '/signup',
        failureFlash    : true
    }));

    app.get('/profile', function(req, res) {
        res.render('main/profile.ejs', {
            user : req.user
        });
    });

    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    })

};