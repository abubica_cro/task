/**
 * Created by xart on 21.01.16..
 */

module.exports = function (app, passport) {

    //home paged
    app.get('/', function(req, res) {
        if (req.isAuthenticated())
        {
            res.redirect('/todoList');
        }
        else
        {
            res.render('main/index.ejs');

        }
    });



};