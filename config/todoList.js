/**
 * Created by xart on 25.01.16..
 */

var mongoose    = require('mongoose'),
    todoList    = mongoose.model('TodoList'),
    task        = mongoose.model('Task');

module.exports = {
    list: {
        showAllLists: function (req, res, next) {
            todoList.find({_userId: req.user._id}, function (err, lists) {
                if (err) {
                    return next(err);
                }
                res.render('todoList/allLists.ejs', {lists: lists});
            });
        },
        createNewList: function (req, res, next) {
            var newList = new todoList();

            newList.listName = req.body.addNewList;
            newList._userId = req.user._id;
            newList.save(function (err) {
                if (err) {
                    return next(err);
                }
            });
            res.redirect('todoList');
        },
        deleteList: function (req, res, next) {
            var listSlug = req.params.listSlug;

            todoList.findOneAndRemove({ listSlug: listSlug, $and: [{ _userId: req.user._id }] }, function (err, oneList) {
                if (err) {
                    return next(err);
                }
                oneList.remove();
                res.redirect('/todoList');
            });
        }
    },
    task: {
        showAllTasks: function (req, res, next) {
            var listSlug = req.params.listSlug;
            todoList.findOne({ _userId: req.user._id, $and: [{ listSlug: listSlug}]}, function (err, oneList) {
                if (err) {
                    return next(err);
                }

                if (oneList) {
                    task.find({_listId: oneList._id}).sort('taskPriority').exec(function (err, tasks) {
                        if (err) {
                            return next(err);
                        }

                        res.render('task/allTasks.ejs', { tasks: tasks, oneList: oneList, message: req.flash('taskMsg') });
                    });
                }
                else {
                    res.redirect('/');
                }
            });
        },
        getTask: function(req, res, next) {
            var taskSlug = req.params.taskSlug;

            task.findOne({ taskSlug: taskSlug}).populate('_listId', 'listSlug').exec(function(err, task) {
                res.render('task/editTask.ejs', { task: task,  message: req.flash('taskMsg')  });
            });

        },
        createTask: function (req, res, next) {
            var newTask = new task();
            newTask.taskName        = req.body.addNewTask;
            newTask.taskPriority    = req.body.taskPriority;
            newTask.taskDeadline    = req.body.taskDeadline;
            newTask._userId         = req.user._id;
            newTask._listId         = req.body.listId;
            newTask.save(function (err) {
                if (err) {
                    return next(err);
                }
            });
            res.redirect('back');
        },
        deleteTask: function (req, res, next) {
            var listSlug = req.params.listSlug,
                id = req.params.id;

            todoList.findOne({ _userId: req.user._id, $and: [{ listSlug: listSlug}]}, function (err, oneList) {
                if (err) {
                    return next(err);
                }
                if (oneList) {
                    task.remove({ _id: id, $and: [{ _listId: oneList._id }]}, function (err) {
                        if (err) {
                            return next(err);
                        }
                        req.flash('taskMsg', 'Task Deleted');
                        res.redirect('back');
                    });
                }
                else {
                    req.flash('taskMsg', 'Delete Failed');
                    res.redirect('back');
                }
            });
        },
        updateTask: function(req, res, next) {
            var listSlug = req.params.listSlug,
                taskSlug = req.params.taskSlug,
                userId   = req.user._id;

            task.findOne({ taskSlug: taskSlug, $and: [{ _userId: userId }]}, function(err, oneTask) {
                if (err) {
                    return next(err);
                }
                if(oneTask){
                    oneTask.taskName       = req.body.taskName;
                    oneTask.taskDeadline   = req.body.taskDeadline;
                    oneTask.taskCompleted  = req.body.taskCompleted;

                    oneTask.save(function(err){
                        if (err){
                            return next(err);
                        }
                        res.redirect('/todoList/'+listSlug);
                    });
                } else {
                    req.flash('taskMsg', 'Task Complited');
                    res.redirect('back');
                }
            });

        },
        completeTask: function (req, res, next) {
            var listSlug = req.params.listSlug,
                taskId = req.params.id,
                userId = req.user._id;

            todoList.findOne({ _userId: userId, $and: [{ listSlug: listSlug}]}, function (err, oneTask) {
                if (err) {
                    return next(err);
                }
                if (oneTask) {
                    task.findByIdAndUpdate(taskId, { $set: {'taskCompleted': 1 }}, function(err) {
                        if (err) {
                            return next(err);
                        }
                        req.flash('taskMsg', 'Task Complited');
                        res.redirect('back');
                    });
                }
                else {
                    res.redirect('/todoList/' + listSlug);
                }
            });
        }
    }
};

