/**
 * Created by xart on 19.01.16..
 */
var LocalStrategy   = require('passport-local').Strategy;

var User            = require('../app/models/user');

module.exports = function(passport) {

    //session setup
    //serialize user
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    //deserialize user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    //signup
    passport.use('local-signup', new LocalStrategy({
        usernameField       : 'email',
        passwordField       : 'password',
        passReqToCallback   : true
    }, function(req, email, password, done) {
        //nextTick won't start unless data is send back
        process.nextTick(function() {
            //check email if it exsists
            User.findOne({'local.email' : email }, function(err, user) {

                if (err)
                    return done(err);

                if (user) {
                    return done(null, false, req.flash('signupMessage', 'Email already in use.'));
                } else {
                    var newUser             = new User();

                    newUser.local.firstName = req.body.firstName;
                    newUser.local.lastName  = req.body.lastName;
                    newUser.local.email     = email;
                    newUser.local.password  = newUser.generateHash(password);
                    //save user
                    newUser.save(function(err) {

                        if (err)//todo rijesi throw error http://stackoverflow.com/questions/15711127/express-passport-node-js-error-handling
                            throw err;
                        return done(null, newUser);
                    });
                }
            });
        });
    }));

    //login
    passport.use('local-login', new LocalStrategy({

        usernameField       : 'email',
        passwordField       : 'password',
        passReqToCallback   : true
    },
        function(req, email, password, done) {

            User.findOne({'local.email' : email }, function(err, user) {

                if(err)
                    return done(err);

                if(!user)
                    return done(null, false, req.flash('loginMessage', 'No user found.'));

                if(!user.validPassword(password))
                    return done(null, false, req.flash('loginMessage', 'Wrong password'));

                return done(null, user);
            });
        }));
};